@extends('base')

@section('top-white')
    <h1>Donors</h1>
@stop

@section('content-title')

@stop

@php
    $type = ['0' => 'General', '1' => 'Jim Myles Auditorium'];
@endphp

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Donors</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <table id="itable" class="display table table-compact table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Date/Time</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Type</th>
                        <th>For</th>
                        <th>Amount</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Province</th>
                        <th>Postal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($donors as $donor)
                        <tr>
                            <td>{!! $donor->created_at !!}</td>
                            <td>{!! $donor->last_name !!}, {!! $donor->first_name !!}</td>
                            <td>{!! $donor->email !!}</td>
                            <td>{!! $donor->donation_type !!}</td>
                            <td>{!! $type[$donor->jim_myles] !!}</td>
                            <td>{!! number_format($donor->donation_amount,2) !!}</td>
                            <td>{!! $donor->address !!}</td>
                            <td>{!! $donor->city !!}</td>
                            <td>{!! $donor->province !!}</td>
                            <td>{!! $donor->zip !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('bottom-js')
    <script>
        $(document).ready(function() {
            $('#itable').dataTable({
                responsive: true,
                saveState: true,
            });
        });
    </script>
@stop