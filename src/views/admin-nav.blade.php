@if (Auth::user()->hasRole('pages'))
    @if (Request::segment(2) == 'donors')
        <li class='active'>
    @else
        <li>
        @endif
        <a href="#"><i class="fa fa-usd"></i> <span class="nav-label">Donors</span><span
                    class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="/admin/donors">Donor's List</a></li>
            <li><a href="/admin/donors/excel">Donor's List as Excel</a></li>
        </ul>
    </li>
@endif