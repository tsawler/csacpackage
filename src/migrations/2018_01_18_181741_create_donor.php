<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonor extends Migration
{
    public function up()
    {
        Schema::create('donors', function ($table)
        {
            $table->increments('id');
            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address');
            $table->string('city');
            $table->string('province');
            $table->string('zip');
            $table->string('donation_type');
            $table->string('donation_amount');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('donors');
    }
}
