<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDonorsTable extends Migration
{
    public function up()
    {
        Schema::table('donors', function($table)
        {
            $table->integer('jim_myles')->default(0);
            });
    }

    public function down()
    {

    }
}
