<?php
Route::post('/csac/process-donation', 'Tsawler\CSACPackage\PurchaseController@postCharge');
Route::post('/csac/process-donation-monthly', 'Tsawler\CSACPackage\PurchaseController@postRecurringCharge');

Route::group(['middleware' => ['web']], function () {
    Route::group(['middleware' => 'auth.pages'], function () {
        Route::get('/admin/donors', 'Tsawler\CSACPackage\DonorController@getDonors');
        Route::get('/admin/donors/excel', 'Tsawler\CSACPackage\DonorController@getExcel');
    });
});
