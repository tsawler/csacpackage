<?php namespace Tsawler\CSACPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Stripe;

/**
 * Class PurchaseController
 * @package App\Http\Controllers
 */
class PurchaseController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCharge()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $amt = Input::get('amount');

        $message = "";
        $passed = true;

        try {
            $customer = Customer::create([
                'email'  => request('stripeEmail'),
                'source' => request('stripeToken'),
            ]);

            Charge::create([
                'customer' => $customer->id,
                'amount'   => $amt,
                'currency' => 'cad',
            ]);

            $donor = new Donor;
            $donor->email = Input::get('stripeEmail');
            $donor->first_name = Input::get('first_name');
            $donor->last_name = Input::get('last_name');
            $donor->address = Input::get('address');
            $donor->city = Input::get('city');
            $donor->province = Input::get('province');
            $donor->zip = Input::get('zip');
            $donor->donation_type = "One Time";
            $donor->jim_myles = Input::get('jim_myles');
            $donor->donation_amount = number_format(($amt / 100), 2);
            $donor->save();

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $passed = false;
        }

        return response()->json(['status' => $passed, 'message' => $message]);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRecurringCharge()
    {

        // get details to create plan
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $amt = Input::get('amount');

        $message = "";
        $passed = true;

        $plan = null;

        try {
            $plan = Plan::retrieve("plan_" . $amt);
        } catch (\Exception $e) {
            $plan = Plan::create([
                'amount'   => $amt,
                'interval' => 'month',
                'currency' => 'cad',
                'id'       => "plan_" . $amt,
                'name'     => 'Monthly donation to CSAC',
            ]);
        }

        try {
            $customer = Customer::create([
                'email'  => request('stripeEmail'),
                'source' => request('stripeToken'),
                'plan'   => $plan->id,
            ]);

            $donor = new Donor;
            $donor->email = Input::get('stripeEmail');
            $donor->first_name = Input::get('first_name');
            $donor->last_name = Input::get('last_name');
            $donor->address = Input::get('address');
            $donor->city = Input::get('city');
            $donor->province = Input::get('province');
            $donor->zip = Input::get('zip');
            $donor->donation_type = "Recurring";
            $donor->donation_amount = number_format(($amt / 100), 2);
            $donor->jim_myles = Input::get('jim_myles');
            $donor->save();


        } catch (\Exception $e) {
            $message = $e->getMessage();
            $passed = false;
        }

        return response()->json(['status' => $passed, 'message' => $message]);
    }

}
