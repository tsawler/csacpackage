<?php namespace Tsawler\CSACPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class DonorController
 * @package Tsawler\CSACPackage
 */
class DonorController extends Controller
{


    /**
     * Show calendar in admin tool
     *
     * @return mixed
     */
    public function getDonors()
    {
        $donors = Donor::orderBy('created_at', 'desc')->get();

        return View::make('csacpackage::admin.donors-all')
            ->with('donors', $donors);
    }

    /**
     *
     */
    public function getExcel()
    {
        $data = Donor::orderBy('last_name')->get();
        $type = ['0' => 'General', '1' => 'Jim Myles Auditorium'];

        $final = [
            ["Donor Report for " . date("Y") . ", generated on " . date("Y-m-d")],
            [
                "Last Name",
                "First Name",
                "Email",
                "Address",
                "City",
                "Postal Code",
                "Donation Type",
                "For",
                "Donation Amount",
            ]];

        foreach ($data as $item) {
            $final[] = [
                $item->last_name,
                $item->first_name,
                $item->email,
                $item->address,
                $item->city,
                $item->zip,
                $item->donation_type,
                $type[$item->jim_myles],
                $item->donation_amount];
        }

        Excel::create('Donation Report', function ($excel) use ($final) {
            $excel->sheet('Sheet 1', function ($sheet) use ($final) {
                $sheet->fromArray($final);
            });
        })->download('xlsx');
    }

}
