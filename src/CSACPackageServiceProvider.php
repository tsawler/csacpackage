<?php namespace Tsawler\CSACPackage;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

/**
 * Class CSACPackageServiceProvider
 * @package Tsawler\CSACPackage
 */
class CSACPackageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *c
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadViewsFrom(__DIR__ . '/views', 'csacpackage');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}
